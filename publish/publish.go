package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	mpub "bitbucket.org/newyuinc/platform-bus/pkg/publisher"
	"github.com/google/uuid"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Flags = initializeFlags()

	app.Action = publishMessages

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func initializeFlags() (flags []cli.Flag) {
	flags = append(flags, &cli.IntFlag{
		Name:  "num-messages",
		Value: 1,
		Usage: "Number of messages to send.  Default is 1",
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "topic",
		Value: "testaddress",
		Usage: "topic of the message to send",
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "message",
		Usage: "Optional message to use.  If not specified, will generate random messages",
		Value: `{"message": "Hello"}`,
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "filepath",
		Usage: "Optional file to send as a message",
	})

	return flags
}

func publishMessages(c *cli.Context) error {

	cfg := &mpub.Configuration{
		UserAgent: "mbus-publisher",
		NsqdNode:  "phoenix-nsq:4150",
	}

	p, err := mpub.New("local", "mbus-pub", cfg)
	if err != nil {
		return err
	}

	n := c.Int("num-messages")
	t := c.String("topic")
	m := c.String("message")
	f := c.String("filepath")

	var data map[string]interface{}
	var mBytes []byte
	if f != "" {
		mBytes, err = ioutil.ReadFile(f)
		if err != nil {
			return err
		}
	} else {
		mBytes = []byte(m)
	}

	err = json.Unmarshal(mBytes, &data)
	if err != nil {
		return err
	}

	for i := 0; i < n; i++ {
		err = p.PublishMessage(uuid.New().String(), t, data)
		if err != nil {
			return err
		}
	}

	return nil
}
