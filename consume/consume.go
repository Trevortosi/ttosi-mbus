package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	msub "bitbucket.org/newyuinc/platform-bus/pkg/consumer"
	echosvc "bitbucket.org/newyuinc/platform-echo/pkg/service"
)

type Address struct {
	City    string `json:"city"`
	Country string `json:"country"`
	State   string `json:"state"`
	Zip     string `json:"zip"`
}

type AddressHandler struct{}

func (h *AddressHandler) ProvideInstance() interface{} {
	return new(Address)
}

func (h *AddressHandler) HandleMessage(m *msub.MessageInstance) error {

	msg, ok := m.Data.(*Address)
	if !ok {
		return fmt.Errorf("couldNotDeserializeMsg")
	}

	if msg.Country == "Narnia" {
		return fmt.Errorf("No Narnians allowed")
	}

	fmt.Printf("%+v\n", msg)

	return nil
}

func main() {
	ah := &AddressHandler{}

	cfg := &msub.Configuration{
		LookupdNodes: []string{"http://phoenix-nsq:4161"},
		UserAgent:    "test-consumer",
		API:          &echosvc.APIClientConfiguration{URL: "http://phoenix-mbus:5003"},
	}
	cfg.AddSubscription("testaddress", "testchan", 1, ah, ah)

	c, err := msub.New("user", "test-consumer", cfg)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.Start()
	if err != nil {
		fmt.Println(err)
		return
	}

	shutdown := make(chan os.Signal, 2)
	signal.Notify(shutdown, syscall.SIGINT)

	for {
		select {
		case <-shutdown:
			c.Stop()
			return
		}
	}
}
