module ttosi-mbus

go 1.12

require (
	bitbucket.org/newyuinc/ginto v0.0.0-20190826214336-bc4b892af2d9 // indirect
	bitbucket.org/newyuinc/platform-bus v0.0.0-20190930151818-4f42a0ff9c77
	bitbucket.org/newyuinc/platform-echo v0.0.0-20190903190959-eff8fa26e0e5
	bitbucket.org/newyuinc/platform-insights v0.0.0-20190903191100-1d214084e9fc // indirect
	bitbucket.org/newyuinc/platform-secrets v0.0.0-20190826220004-b8b07ee72b65 // indirect
	github.com/alexcesaro/statsd v2.0.0+incompatible // indirect
	github.com/aws/aws-sdk-go v1.25.37 // indirect
	github.com/coreos/etcd v3.3.17+incompatible // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/dgryski/go-lttb v0.0.0-20180810165845-318fcdf10a77 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lithammer/go-jump-consistent-hash v1.0.1 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/moshenahmias/failure v0.0.0-20180520123008-c31a9a4c0318 // indirect
	github.com/nsqio/go-nsq v1.0.7 // indirect
	github.com/urfave/cli v1.22.1
	github.com/valyala/fasttemplate v1.1.0 // indirect
	go.uber.org/zap v1.13.0 // indirect
	google.golang.org/grpc v1.25.1 // indirect
)
